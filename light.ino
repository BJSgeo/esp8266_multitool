/*
   light - This is the program for a smart home light that works
   with Amazon Alexa and my multitool.

   I copied a lot from the fauxmoESP example.

   Copyright (c) 2019 Ben Goldberg
*/
#include <fauxmoESP.h>
#include <ESP8266WiFi.h>
#define LIGHT_1 "light"
#define LIGHT_1_PORT D1
fauxmoESP light;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  WiFi.mode(WIFI_STA);
  pinMode(LIGHT_1_PORT, OUTPUT);
  WiFi.begin("SSID", "password");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.print("ok cool, IP: ");
  Serial.println(WiFi.localIP());
  Serial.println("setting up fauxmo now");
  light.createServer(true);
  light.setPort(80);
  light.enable(true);
  Serial.println("fauxmo enabled");
  light.addDevice(LIGHT_1);
  light.onSetState([](unsigned char device_id, const char * device_name, bool state, unsigned char value) {
    if (state == 0) {
      value = 0;
    }
    if (strcmp(device_name, LIGHT_1) == 0) {
      analogWrite(LIGHT_1_PORT, value);
    }
    Serial.println("received");
  });
}

void loop() {
  // put your main code here, to run repeatedly:
  light.handle();
  delay(540);
}
