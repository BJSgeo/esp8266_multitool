// include libraries
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
#include <ESP8266mDNS.h>
#include <ESP8266WiFiMulti.h>
// global variables
ESP8266WiFiMulti wifiMulti;
int value255;
int read;
int time1;
volatile bool WEATHER, MEASURE, SMART_HOME, HOT_COLD = false;
bool hCConn = false;
#define BUTTON D3
/*
   ESP8266_multitool.ino -- an internet-connected multitool! To get weather,
   add cities in the list and their corresponding GPS coordinates. The smart home works
   with my light.ino Arduino sketch which emulates a Philips Hue lightbulb.
   You can also measure distance with an ultrasonic sensor or play
   high-tech hotter/colder (with my HotColdTransmit Arduino sketch also
   running on an ESP8266 -- you can also have a Wi-Fi network called Hotter/Colder)
   NOTES:
    Change all instances of 1.2.3.4 to your smart home light's IP address (maybe use replace all)
   USAGE:
    You will get prompted to select a mode -- at one end of the dial is weather,
    the other end is controlling a smart home light, anywhere from the middle of the dial
    to the light is hotter/colder, and anywhere from the weather to the middle of the dial
    is measuring with an ultrasonic sensor
   CIRCUITRY and Bill of Materials:
    BOM first:
      ESP-12E (NodeMCU) is the development board
      You need a potentiometer too
      You need a display also - I used https://www.adafruit.com/product/2157 and the multitool will work
      with that display in any color.
      USB battery pack
      USB cable (I added a switch to a USB cable so I can turn the multitool on or off. 
      I used these switches: https://www.amazon.com/dp/B07D28XPKV/ )
      Also an ultrasonic sensor (specifically the HC-SR04 or compatible)

    And now the circuits:
      Potentiometer (dial) with 3 pins - 1 end to 3v3, other end to GND, middle to A0

      Wire both power pins of the display (VCC+Vi2c) to 3v3, ground to GND,
      clock/SCL to D1, data/SDA to D2

      HC-SR04 (ultrasonic) -- VCC to VIN (next to GND near the reset button), TRIG to D6,
      ECHO---2.2 kilo-ohm resistor------3.3 kilo-ohm resistor---GND
                                     |
                                     D5
      and GND to GND (if you're wondering what the resistors are for, it's a voltage divider.
      You need one because the ultrasonic sensor uses 5V and the ESP8266 uses 3.3V so we need
      to step it down.)

   Copyright (c) 2019 Ben Goldberg
*/
// to control the display
Adafruit_AlphaNum4 alpha4 = Adafruit_AlphaNum4();
// HC-SR04 pins
#define ECHO D5
#define TRIG D6
// different cities for checking weather
// and their locations
// I set them all to Washington, DC in the USA but feel free to change
// The last one is a test to show you what different locations look like
const String cities[] = {"DC  ", "DC  ", "DC  ", "Test"};
const String locations[] = {"38.9051,-77.0372", "38.9051,-77.0372",
                            "38.9051,-77.0372", "00.0000,00.0000"
                           };
// so the degree symbol is not in the Adafruit_LEDBackpack library,
// so I had to add it as the ` character. I chose ` because it is not
// displayed (or useful) in this program.
const char degsym[] = {'`'};
// the fingerprint of the certificate for https://api.weather.gov
// Sometimes it changes, last changed 8/5/2019, will change again
// around November 4, 2020
const char fingerprint[] PROGMEM = "29 f0 37 ba ce 86 3c 94 3c 3b df 2c e5 30 3a 24 3e ae f4 91";
void setup() {
  // this runs once.
  // Set up the display on I2C address hexadecimal 70 because that's
  // the address the display uses by default.
  alpha4.begin(0x70);
  // starts a serial connection over USB
  Serial.begin(115200);
  // Set up ultrasonic sensor
  pinMode(ECHO, INPUT);
  pinMode(TRIG, OUTPUT);
  // Set up potentiometer (on A0)
  pinMode(A0, INPUT);
  // set up button
  pinMode(BUTTON, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON), button, CHANGE);
  // ESP8266, you're supposed to be on a WiFi network! (8266 is now a station, hence WiFi_STA(tion))
  WiFi.mode(WIFI_STA);
  // Connect!
  wifiMulti.addAP("WiFi access point #1 - SSID", "WiFi access point #1 - password");
  wifiMulti.addAP("WiFi access point #2 - SSID", "WiFi access point #2 - password");
  // you can add more networks if you want with wifiMulti.addAP("SSID", "password")
  // set a timer so WiFi connection can't take longer than 6000ms (6 seconds)
  unsigned long t = millis();
  while ((wifiMulti.run() != WL_CONNECTED) && millis() - t <= 6000) {
    delay(500);
    Serial.print(".");
  }
  if (wifiMulti.run() != WL_CONNECTED) {
    // oh no, no WiFi! :(
    scroll("!! No Wi-Fi !!    ", 2);
  }
  // set up an mDNS responder so we can (hopefully) connect to the multitool
  // at multitool.local
  if (!MDNS.begin("multitool")) {
    Serial.println("Error setting up MDNS responder!");
  }
  // say we have a web server on even though we don't
  MDNS.addService("http", "tcp", 80);
  // and to double check set the hostname to multitool
  WiFi.hostname("multitool");
  // print out how strong our signal is (Received Signal Strength Indicator)
  Serial.println(WiFi.RSSI());
  // scroll that it's my code (copyright)
  scroll("(c) 2019 Ben Goldberg   ", 1);
  // display brightness timer
  time1 = millis();
}

void loop() {
  // check if you need to switch networks but only
  // if hot/cold isn't running
  if (!HOT_COLD) wifiMulti.run();
  // more display brightness stuff
  int old_read = read;
  read = analogRead(A0);
  // has the dial position changed by more than 15
  // since we last checked?
  if (abs(old_read - read) > 15) {
    // if so, reset the display brightness timer
    time1 = millis();
    // and full brightness (don't know if 255 is right but it seems to work)
    alpha4.setBrightness(255);
  }
  if ((millis() - time1) > 18000) {
    // if it's been more than 18 seconds since the timer was set,
    alpha4.setBrightness(2);
    // low brightness ensues.
  }
  // mDNS stuff
  MDNS.update();
  // check mode
  if (WEATHER) {
    // do weather stuff
    scroll("Load", 1);
    getWeather();
    delay(1000);
  }
  else if (MEASURE) {
    // oh, you want to measure huh?
    // ok fine. Pulse the TRIG pin
    digitalWrite(TRIG, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG, LOW);
    // measure how long the sound takes
    unsigned long duration = pulseIn(ECHO, HIGH);
    // and convert it to inches (74.blah blah is about how many microseconds it
    // takes for sound to travel an inch). we divide by 2 since it's there and back.
    float inches = duration / 74.052478134110763855196389259585F / 2.0; // about 74 uS/in is speed of sound
    // and write the value!
    char str[6];
    String(inches, 3).toCharArray(str, 6);
    int8_t dispCounter = 0;
    for (int i = 0; i <= 4; i++) {
      Serial.print("#");
      Serial.print(dispCounter);
      Serial.print(": ");
      if (str[i] == '.') {
        alpha4.writeDigitAscii(dispCounter - 1, str[i - 1], true);
        Serial.print('.');
        dispCounter--;
      } else {
        alpha4.writeDigitAscii(dispCounter, str[i], false);
        Serial.print(str[i]);
      }
      Serial.println();
      dispCounter++;
    }
    Serial.println();
    // update display:
    alpha4.writeDisplay();
    // wait ~1.8 seconds
    delay(1800);
  }
  else if (SMART_HOME) {
    // smart home
    // read dial (take 900?!? samples to make an average)
    int avg = 0;
    for (int i = 0; i < 900; i++) {
      avg += analogRead(A0);
    }
    avg /= 900;
    // 0 to 1024 is raw
    // change from [0-1024] to [0-255] to get value255
    int old_value255 = value255;
    value255 = map(avg, 0, 1024, 0, 255);
    // and then divide the [0-255] by 2.55 to get [0-100]
    int value100 = round(value255 / 2.55);
    // if the difference between old 255 and new 255 is ≥ 2
    if (abs(old_value255 - value255) >= 2) {
      // print out the 0-100
      Serial.println(value100);
      // send it to the display too
      char str[4] = {' ', ' ', ' ', ' '};
      String(value100).toCharArray(str, 4);
      str[3] = '%';
      for (int i = 0; i <= 3; i++) {
        if (str[i] == '.') {
          alpha4.writeDigitAscii(i - 1, str[i - 1], true);
          Serial.print('.');
        } else {
          alpha4.writeDigitAscii(i, str[i]);
          Serial.print(str[i]);
        }
        alpha4.writeDisplay();
        // make a wifi client to connect to the light
        WiFiClient light;
        // connect and print if it fails or not
        Serial.println(light.connect("1.2.3.4", 80));
        // make our data string to send to the light.
        // if it's on, that will look like
        // {"on":true, "bri":[0-255 brightness]}
        // off will look like {"on":false, "bri":0}
        String d;
        if (value255 == 0) {
          d = "{\"on\":false, \"bri\":0}";
        } else {
          d = "{\"on\":true, \"bri\":" + String(value255) + "}";
        }
        // ready for the HTTP request?
        // PUT /api/lights/1/state HTTP/1.1
        // Accept-Encoding: identity
        // Content-Length: <length of data>
        // Host: 1.2.3.4
        // Content-Type: application/x-www-form-urlencoded
        // Connection: close
        // User-Agent: SMART_HOME_CONTROLLER
        //
        // Data data and more data
        light.print(String("PUT /api/lights/1/state HTTP/1.1\nAccept-Encoding: identity\n") + String("Content-Length: ") + String(d.length()) + String("\nHost: 1.2.3.4\nContent-Type: application/x-www-form-urlencoded\nConnection: close\nUser-Agent: SMART_HOME_CONTROLLER\n\n") + d);
        // and also print that to the Serial Monitor
        Serial.println();
        Serial.println();
        Serial.print(String("PUT /api/lights/1/state HTTP/1.1\nAccept-Encoding: identity\n") + String("Content-Length: ") + String(d.length()) + String("\nHost: 1.2.3.4\nContent-Type: application/x-www-form-urlencoded\nConnection: close\nUser-Agent: SMART_HOME_CONTROLLER\n\n") + d);
        Serial.print(d);
        // wait for response (3.6 seconds or less)
        int counter = 0;
        while (light.available() == 0) {
          Serial.print(".");
          delay(100);
          counter += 1;
          if (counter > 36) {
            return;
          }
        }
        // then send response to the serial monitor
        while (light.available()) {
          char ch = static_cast<char>(light.read());
          Serial.print(ch);
        }
        // close the connection
        light.stop();
      }
    }
  } else if (HOT_COLD) {
    if (WiFi.SSID() != "Hotter/Colder" && !hCConn) {
      WiFi.mode(WIFI_STA);
      WiFi.begin("Hotter/Colder");
      hCConn = true;
    } else if (WiFi.SSID() == "Hotter/Colder") {
      uint8_t currentRSSI = map(WiFi.RSSI(), -95, -25, 0, 18);
      char buf[4];
      itoa(currentRSSI, buf, 10);
      for (int i = 0; i <= 3; i++) {
        if (buf[i] == '.') {
          alpha4.writeDigitAscii(i - 1, buf[i - 1], true);
        } else {
          alpha4.writeDigitAscii(i, buf[i]);
          Serial.print(buf[i]);
        }
      }
      alpha4.writeDisplay();
    }
    delay(180);
  } else {
    WEATHER, MEASURE, SMART_HOME, HOT_COLD = false;
    // select mode.
    // One end is smart home.
    // Other end is weather.
    // In the middle is measure.
    scroll("Select mode by turning dial    ", 1);
    delay(1000);
    int read = analogRead(A0);
    if (read == 0) {
      WEATHER = true;
      scroll("Weather coming up!", 1);
    }
    if (read > 0 && read < 512) {
      MEASURE = true;
      scroll("Measure coming up!", 1);
    }
    if (read > 511 && read < 1024) {
      scroll("Let's play Hotter/Colder!", 1);
      HOT_COLD = true;
    }
    if (read == 1024) {
      SMART_HOME = true;
      scroll("Smart home coming up!", 1);
    }
  }
}
// function for scrolling onto the display
char displaybuffer[4] = {' ', ' ', ' ', ' '};

void scroll(const char *data, int iter) {
  for (iter; iter > 0; iter--) {
    for (int x = 0; x < 280; x++) {
      char c = data[x];
      if (! isprint(c)) return;
      // scroll down display
      displaybuffer[0] = displaybuffer[1];
      displaybuffer[1] = displaybuffer[2];
      displaybuffer[2] = displaybuffer[3];
      displaybuffer[3] = c;

      // set every digit to the buffer
      for (int i = 0; i <= 3; i++) {
        if (displaybuffer[i] == '.') {
          alpha4.writeDigitAscii(i, ' ', true);
        } else {
          alpha4.writeDigitAscii(i, displaybuffer[i]);
        }
        if (displaybuffer[i] == degsym[0]) {
          alpha4.writeDigitRaw(i, 0x00E3);
        }
      }

      // write it out!
      alpha4.writeDisplay();
      delay(200);
    }
  }
}
// Second weather function
void getForecast(const char* url) {
  Serial.print(F("Heap free: "));
  Serial.println(ESP.getFreeHeap());
  String url2 = String(url);
  WiFiClientSecure client;
  client.setTimeout(10000);
  client.setFingerprint(fingerprint);
  if (!client.connect("api.weather.gov", 443)) {
    Serial.println(F("Connection failed"));
    return;
  }

  Serial.println(F("Connected!"));
  scroll(".", 1);

  // Send HTTP request
  client.println(String("GET ") + url2 + String(" HTTP/1.1"));
  client.println(F("Host: api.weather.gov"));
  client.println(F("User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    return;
  }

  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }
  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return;
  }

  // Allocate the JSON document
  // Use arduinojson.org/v6/assistant to compute the capacity.
  const size_t capacity = JSON_ARRAY_SIZE(1) + 8 * JSON_ARRAY_SIZE(2) + JSON_ARRAY_SIZE(5) + JSON_ARRAY_SIZE(14) + 4 * JSON_OBJECT_SIZE(2) + 2 * JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(8) + 14 * JSON_OBJECT_SIZE(13) + 7100;
  DynamicJsonDocument doc(capacity);
  Serial.print(F("Heap free -------before parsing-----: "));
  Serial.println(ESP.getFreeHeap());
  // Parse JSON object
  DeserializationError error = deserializeJson(doc, client);
  client.stop();
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  scroll(".", 1);
  JsonObject properties = doc["properties"];
  JsonArray properties_periods = properties["periods"];
  JsonObject properties_periods_0 = properties_periods[0];
  JsonObject properties_periods_1 = properties_periods[1];
  JsonObject properties_periods_2 = properties_periods[2];
  char temp[3];
  properties.clear();
  doc.clear();
  // replace short with detailed for long
  // 0
  Serial.print((const char*)properties_periods_0["name"]);
  Serial.print(": ");
  Serial.println((const char*)properties_periods_0["shortForecast"]);
  // 1
  Serial.print((const char*)properties_periods_1["name"]);
  Serial.print(": ");
  Serial.println((const char*)properties_periods_1["shortForecast"]);
  // 2
  Serial.print((const char*)properties_periods_2["name"]);
  Serial.print(": ");
  Serial.println((const char*)properties_periods_2["shortForecast"]);
  scroll("Here's the weather:    ", 1);

  for (int i = 0; i < 3; i++) {
    itoa(properties_periods[i]["temperature"], temp, 10);
    scroll((const char*)properties_periods[i]["name"], 1);
    scroll(": ", 1);
    scroll((const char*)properties_periods[i]["shortForecast"], 1);
    scroll(", ", 1);
    scroll((bool)properties_periods[i]["isDaytime"] ? "high of " : "low of ", 1);
    scroll(temp, 1);
    scroll(degsym, 1); // ` marks deg symbol
    scroll((const char*)properties_periods[i]["temperatureUnit"], 1);
    scroll("    ", 1);
  }
  doc.clear();
  properties.clear();
}
// First weather function
void getWeather() {
  Serial.print(F("Heap free: "));
  Serial.println(ESP.getFreeHeap());
  int value = analogRead(A0);
  int location = 0;
  if (value == 1024) {
    scroll(cities[3].c_str(), 1);
    delay(1000);
    location = 3;
  } else if (value >= 512) {
    scroll(cities[2].c_str(), 1);
    delay(1000);
    location = 2;
  } else if (value == 0) {
    scroll(cities[0].c_str(), 1);
    delay(1000);
    location = 0;
  } else if (value < 512) {
    scroll(cities[1].c_str(), 1);
    delay(1000);
    location = 1;
  }
  WiFiClientSecure client;
  client.setTimeout(10000);
  client.setFingerprint(fingerprint);
  if (!client.connect("api.weather.gov", 443)) {
    Serial.println(F("Connection failed"));
    return;
  }

  Serial.println(F("Connected!"));
  scroll(".", 1);
  // Send HTTP request
  client.println(String("GET /points/") + locations[location] + String(" HTTP/1.1"));
  client.println(F("Host: api.weather.gov"));
  client.println(F("User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    return;
  }

  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }

  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return;
  }

  // Allocate the JSON document
  // Use arduinojson.org/v6/assistant to compute the capacity.
  const size_t capacity = 3 * JSON_ARRAY_SIZE(2) + 6 * JSON_OBJECT_SIZE(1) + 7 * JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5) + 2 * JSON_OBJECT_SIZE(16) + 1650;
  DynamicJsonDocument doc(capacity);

  // Parse JSON object
  DeserializationError error = deserializeJson(doc, client);
  client.stop();
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  JsonObject properties = doc["properties"];
  const char* forecast = properties["forecast"];
  Serial.println(forecast);
  doc.clear();
  properties.clear();
  scroll(".", 1);
  getForecast(forecast);
}

// function for setting to no mode so
// select will kick in
ICACHE_RAM_ATTR void button() {
  WEATHER = false;
  MEASURE = false;
  SMART_HOME = false;
  HOT_COLD = false;
}
